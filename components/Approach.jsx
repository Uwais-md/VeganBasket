import React from "react";
import Image from "next/image";
import Farm from "../assets/farmhouse.png";
import Support from "../assets/support.png";
import Quality from "../assets/quality.png";
import { useInView } from 'react-intersection-observer';
import { useSpring, animated } from 'react-spring';
import classNames from 'classnames';


const Approach = () => {

  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.5,
  });

  const fadeIn = useSpring({
    opacity: inView ? 1 : 0,
    from: { opacity: 0 },
    config: { duration: 1000 }, // Adjust the duration as needed
  });


  const animatedClass = classNames({
    'animate-fade-out': inView,
    // Add other classes as needed
  });

  return (
    <animated.div
      id='approach'
      className={`${animatedClass}`}
      ref={ref}
      style={fadeIn}
    >
      <h1 className=" font-bold text-4xl text-center mt-10">OUR UNIQUE APPROACH</h1>
    <div className=" flex flex-col lg:flex-row items-center my-5 z-50">
      <div className="flex flex-col lg:flex-row mt-10 md:mt-20 gap-y-10 md:gap-x-48 md:justify-center">
        <div className="w-64 md:w-auto md:mx-5">
          <Image src={Farm} alt="Farm" className="w-24 h-24 mx-auto"></Image>
          <div className="font-semibold text-3xl my-6 text-center">
            Farm-to-Table Goodness
          </div>
          <p className="font-light text-center">
            We bridge the gap between rural farms and your urban home. By
            eliminating unnecessary intermediaries, we ensure that you receive
            the freshest products at prices that won't break the bank.
          </p>
        </div>

        <div className="w-64 md:w-auto md:mx-5">
          <Image src={Support} alt="Farm" className="w-24 h-24 mx-auto"></Image>
          <div className="font-semibold text-3xl my-6 text-center">
            Supporting Local Agriculture
          </div>
          <p className="font-light text-center">
            Your choice to shop with us directly contributes to the livelihoods
            of local farmers. With each purchase, you're empowering sustainable
            agriculture and helping communities thrive.
          </p>
        </div>

        <div className="w-64 md:w-auto md:mx-5">
          <Image src={Quality} alt="Farm" className="w-24 h-24 mx-auto"></Image>
          <div className="font-semibold text-3xl my-6 text-center">
            Quality and Freshness
          </div>
          <p className="font-light text-center">
            Our vegan baskets are curated with care, containing only the highest
            quality fruits and vegetables. You can trust that every bite is
            bursting with flavor and nutrients.
          </p>
        </div>
      </div>
    </div>

    </animated.div>
  );
};

export default Approach;
