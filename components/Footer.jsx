import React from "react";
import Image from "next/image";
import FooterBG from "../assets/footer.png";
import Media from "../assets/media.png";
import { useInView } from 'react-intersection-observer';
import { useSpring, animated } from 'react-spring';
import classNames from 'classnames';


const Footer = () => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.5,
  });

  const fadeIn = useSpring({
    opacity: inView ? 1 : 0,
    from: { opacity: 0 },
    config: { duration: 1000 }, // Adjust the duration as needed
  });


  const animatedClass = classNames({
    'animate-fade-out': inView,
    // Add other classes as needed
  });
  return (
    <animated.div
      id='contact'
      className={`${animatedClass}`}
      ref={ref}
      style={fadeIn}
    >
      <div className="relative w-full h-full md:w-full md:h-full">
        <Image
          src={FooterBG}
          alt="FooterBG"
          className=" z-0 object-cover w-full lg:h-96 sm:h-[600px] h-[800px]"
        />
      <div className="absolute top-0 left-0 right-0 z-10 flex flex-col md:flex-row justify-center lg:justify-around lg:ml-36 items-center md:gap-20 mr-10 sm:ml-10 pt-10">
        <div className="mt-5 md:mt-0 md:w-1/3">
          <h2 className="my-5 text-2xl font-bold">ADDRESS</h2>
          <address className="font-light text-lg not-italic">
            123 Green Street
            <br />
            Urbanville, Vegantown
            <br />
            Freshland, FL 12345
            <br />
            United Veggies
          </address>
        </div>
        <div className="mt-10 md:w-1/3">
          <h2 className="my-5 text-2xl font-bold">CONNECT WITH US</h2>
          <p className="underline my-2 md:my-5">info@veganbasket</p>
          <button className="bg-primary-green rounded-md text-white px-5 py-4">
            +91 93843 02820
          </button>
        </div>
        <div className="mt-10 md:w-1/3">
          <h2 className="my-5 text-2xl font-bold">FOLLOW US</h2>
          <Image src={Media} alt="Media" className="md:my-0"/>
        </div>
      </div>
      </div>
      </animated.div>
  );
};

export default Footer;
